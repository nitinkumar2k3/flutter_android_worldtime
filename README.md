Please check flutter_timezone branch of this for the code repository.

Flutter is Google�s UI toolkit for building beautiful, natively compiled applications for mobile,
web, and desktop from a single codebase.

This application is written in Flutter which displays the world time.
There are few countries , after selecting that we will get the current time of that country 
and on basis of that the view will change.

Its a REst API call to http://worldtimeapi.org/timezones URL.
From view we have list of countries and we make a request to the server and get the json response.

Few Flutter packages,Stateless widget,StatefulWidget,Concept of background task like async 
and Future etc are widely used in this application.